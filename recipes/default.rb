#
# Cookbook Name:: toslove
# Recipe:: default
#
# Copyright 2014, Suzuki-Yuki
#
# All rights reserved - Do Not Redistribute
#
#--------------------------------------------------------------------
# composerをインストールする
#--------------------------------------------------------------------
execute "composer-install" do
    command "curl -sS https://getcomposer.org/installer | php ;mv composer.phar /usr/local/bin/composer"
    not_if { ::File.exists?("/usr/local/bin/composer")}
end

#--------------------------------------------------------------------
# composerでソフトウェアをアップデートする
#--------------------------------------------------------------------
execute "composer-update" do
    cwd '/vagrant'
    command "php /usr/local/bin/composer update"
    only_if { ::File.exists?("/usr/local/bin/composer")}
end